variable "pve_url" {
  type    = string
  default = ""
}

variable "pve_url_insecure" {
  type    = bool
  default = false
}

variable "pve_username" {
  type    = string
  default = ""
}

variable "pve_password" {
  type    = string
  default = ""
}

variable "pve_node" {
  type    = string
  default = ""
}

source "proxmox" "debian-11" {
  proxmox_url              = var.pve_url
  insecure_skip_tls_verify = var.pve_url_insecure
  username                 = var.pve_username
  password                 = var.pve_password
  node                     = var.pve_node
  pool                     = "Provisioning"

  iso_url          = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.0.0-amd64-netinst.iso"
  iso_checksum     = "sha256:ae6d563d2444665316901fe7091059ac34b8f67ba30f9159f7cef7d2fdc5bf8a"
  iso_storage_pool = "local"

  os              = "l26"
  cpu_type        = "host"
  sockets         = 1
  cores           = 2
  memory          = 1024
  scsi_controller = "virtio-scsi-pci"

  disks {
    disk_size         = "10G"
    format            = "raw"
    storage_pool      = "lvm-ssd"
    storage_pool_type = "lvm-thin"
    type              = "scsi"
  }

  network_adapters {
    bridge   = "vmbr1"
    model    = "virtio"
    vlan_tag = "10"
  }

  cloud_init              = true
  cloud_init_storage_pool = "lvm-ssd"

  boot_wait = "10s"
  boot_command = [
    "<esc><wait>",
    "auto net.ifnames=0 preseed/url=https://gitlab.com/br0.fr/packer-templates/-/raw/main/http/preseed.cfg<wait>",
    "<enter>"
  ]

  ssh_timeout  = "15m"
  ssh_username = "root"
  ssh_password = "RootRoot"

  template_description = format("Debian GNU/Linux 11 (bullseye) - Generated on %s", timestamp())
  template_name        = "tpl-debian-11"

  unmount_iso = true
}

build {
  sources = ["source.proxmox.debian-11"]

  provisioner "shell" {
    environment_vars = ["DEBIAN_FRONTEND=noninteractive"]
    inline = [
      "apt-get update -qq",
      "apt-get dist-upgrade -qq -y -o Dpkg::Options::=\"--force-confdef\" -o Dpkg::Options::=\"--force-confold\"",
      "apt-get install -qq -y cloud-init dirmngr gnupg",
      "rm -rf /etc/cloud/*"
    ]
  }

  provisioner "file" {
    destination = "/etc/cloud/"
    direction   = "upload"
    source      = "files/cloud-init/"
  }

  provisioner "shell" {
    inline = [
      "sed -r 's/PermitRootLogin yes/PermitRootLogin prohibit-password/g' -i /etc/ssh/sshd_config"
    ]
  }
}
